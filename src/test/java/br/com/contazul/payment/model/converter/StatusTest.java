package br.com.contazul.payment.model.converter;

import br.com.contazul.payment.model.bankslip.Status;
import br.com.contazul.payment.validation.InvalidStatusException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThat;

public class StatusTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowInvalidStatusExceptionWhenTryingToCreateStatusFromNullValue() {
        thrown.expect(InvalidStatusException.class);
        thrown.expectMessage("Status ID must not be null.");

        Status.fromId(null);
    }

    @Test
    public void shouldThrowInvalidStatusExceptionWhenTryingToCreateStatusFromInvalidId() {
        Integer invalidStatusId = Integer.MAX_VALUE;

        thrown.expect(InvalidStatusException.class);
        thrown.expectMessage("Status ID " + invalidStatusId + " does not exist.");

        Status.fromId(invalidStatusId);
    }

    @Test
    public void shouldCreateAPendingStatusFromValidId() {
        Integer validId = 1;
        Status status = Status.fromId(validId);

        assertThat(status).isSameAs(Status.PENDING);
    }

}