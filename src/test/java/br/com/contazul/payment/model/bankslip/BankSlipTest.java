package br.com.contazul.payment.model.bankslip;

import br.com.contazul.payment.model.overdue.Fin;
import br.com.contazul.payment.model.overdue.OverdueRates;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Period;

import static org.assertj.core.api.Assertions.assertThat;

public class BankSlipTest {

    private String customer = "Marie Curie";

    private OverdueRates overdueRates = new OverdueRates();

    @Test
    public void shouldCreateBankSlipDetailsWithoutFin() {
        BankSlip bankSlip = nonOverdueBankSlip();
        BankSlipDetail bankSlipDetails = bankSlip.createDetails(overdueRates);

        assertThat(bankSlipDetails.getFin()).isEqualTo(Fin.ZERO);
    }

    @Test
    public void shouldCreateBankSlipDetailsWithLowerInterestRate() {
        BankSlip bankSlip = lowerOverdueBankSlip();
        BankSlipDetail bankSlipDetails = bankSlip.createDetails(overdueRates);

        int periodInDays = 5;
        double amount = 100.0;
        double rate = 0.5;

        int fin = (int)((periodInDays * amount * rate) * 100); // 25000

        assertThat(bankSlipDetails.getFin().getAmountInCents()).isEqualTo(fin);
    }

    @Test
    public void shouldCreateBankSlipDetailsWithHigherInterestRate() {
        BankSlip bankSlip = higherOverdueBankSlip();
        BankSlipDetail bankSlipDetails = bankSlip.createDetails(overdueRates);

        int periodInDays = 15;
        double amount = 100.0;
        double rate = 1.0;

        int fin = (int)((periodInDays * amount * rate) * 100); // 150000

        assertThat(bankSlipDetails.getFin().getAmountInCents()).isEqualTo(fin);
    }

    private BankSlip nonOverdueBankSlip() {
        LocalDate tenDaysAhead = LocalDate.now().plus(Period.ofDays(10));
        return new BankSlip(10000, tenDaysAhead, customer, Status.PENDING);
    }

    private BankSlip lowerOverdueBankSlip() {
        LocalDate fiveDaysPast = LocalDate.now().minus(Period.ofDays(5));
        return new BankSlip(10000, fiveDaysPast, customer, Status.PENDING);
    }

    private BankSlip higherOverdueBankSlip() {
        LocalDate fifteenDaysPast = LocalDate.now().minus(Period.ofDays(15));
        return new BankSlip(10000, fifteenDaysPast, customer, Status.PENDING);
    }

}