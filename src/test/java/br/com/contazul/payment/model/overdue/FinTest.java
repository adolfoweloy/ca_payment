package br.com.contazul.payment.model.overdue;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class FinTest {

    @Test
    public void shouldGetAmountInCentsFromFin() {
        Fin fin = new Fin(new BigDecimal("42.33"));
        assertThat(fin.getAmountInCents()).isEqualTo(4233);
    }

    @Test
    public void shouldGetRoundedValueInCents() {
        Fin fin = new Fin(new BigDecimal("42.337"));
        assertThat(fin.getAmountInCents()).isEqualTo(4234);
    }
}
