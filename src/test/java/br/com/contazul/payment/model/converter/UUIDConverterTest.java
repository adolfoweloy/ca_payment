package br.com.contazul.payment.model.converter;

import br.com.contazul.payment.converter.UUIDConverter;
import org.junit.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UUIDConverterTest {

    private static final String SAMPLE_ID = "26b113fb-949e-419b-80ed-1c4e1d1cefd8";
    private UUIDConverter subject = new UUIDConverter();

    @Test
    public void shouldConvertUUIDFromString() {
        String value = subject.convertToDatabaseColumn(UUID.fromString(SAMPLE_ID));
        assertThat(value).isEqualTo(SAMPLE_ID);
    }

    @Test
    public void shouldConvertUUIDToString() {
        UUID UUID = subject.convertToEntityAttribute(SAMPLE_ID);
        assertThat(UUID.toString()).isEqualTo(SAMPLE_ID);
    }
}