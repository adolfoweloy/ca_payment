package br.com.contazul.payment.model.overdue;

import br.com.contazul.payment.model.bankslip.BankSlip;
import br.com.contazul.payment.model.bankslip.Status;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

import static org.assertj.core.api.Assertions.assertThat;

public class OverdueRatesTest {

    private OverdueRates subject = new OverdueRates();
    private String customer = "Nikola Tesla";

    @Test
    public void shouldCreateNoOverdueRate() {
        OverdueRate overdueRate = subject.createFrom(
                getOverdueInDays(nonOverdueBankSlip().getDueDate()));
        assertThat(overdueRate).isSameAs(OverdueRate.NO_OVERDUE);
        assertThat(overdueRate.getRate()).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    public void shouldCreateLowerOverdueRate() {
        OverdueRate overdueRate = subject.createFrom(
                getOverdueInDays(lowerOverdueBankSlip().getDueDate()));
        assertThat(overdueRate).isSameAs(OverdueRate.LOWER_OVERDUE_RATE);
        assertThat(overdueRate.getRate()).isEqualTo(BigDecimal.valueOf(0.5));
    }

    @Test
    public void shouldCreateHigherOverdueRate() {
        OverdueRate overdueRate = subject.createFrom(
                getOverdueInDays(higherOverdueBankSlip().getDueDate()));
        assertThat(overdueRate).isSameAs(OverdueRate.HIGHER_OVERDUE_RATE);
        assertThat(overdueRate.getRate()).isEqualTo(BigDecimal.valueOf(1.0));
    }

    private BankSlip nonOverdueBankSlip() {
        LocalDate tenDaysAhead = LocalDate.now().plus(Period.ofDays(10));
        return new BankSlip(1000, tenDaysAhead, customer, Status.PENDING);
    }

    private BankSlip lowerOverdueBankSlip() {
        LocalDate fiveDaysPast = LocalDate.now().minus(Period.ofDays(5));
        return new BankSlip(1000, fiveDaysPast, customer, Status.PENDING);
    }

    private BankSlip higherOverdueBankSlip() {
        LocalDate fifteenDaysPast = LocalDate.now().minus(Period.ofDays(15));
        return new BankSlip(1000, fifteenDaysPast, customer, Status.PENDING);
    }

    private int getOverdueInDays(LocalDate dueDate) {
        LocalDate today = LocalDate.now();
        return Period.between(dueDate, today).getDays();
    }
}