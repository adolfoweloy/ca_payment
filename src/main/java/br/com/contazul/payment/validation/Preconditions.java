package br.com.contazul.payment.validation;

import java.util.function.Supplier;

public class Preconditions {

    public static <T, E extends RuntimeException> void requireNonNull(T value, Supplier<E> exception) {
        if (value == null) {
            throw exception.get();
        }
    }

}
