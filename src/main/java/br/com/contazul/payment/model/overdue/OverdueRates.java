package br.com.contazul.payment.model.overdue;

public class OverdueRates {

    private static final int LIMIT_PERIOD = 10;

    public OverdueRate createFrom(int overDueInDays) {
        if (overDueInDays <= 0) {
            return OverdueRate.NO_OVERDUE;
        }

        if (overDueInDays < LIMIT_PERIOD) {
            return OverdueRate.LOWER_OVERDUE_RATE;
        }

        return OverdueRate.HIGHER_OVERDUE_RATE;
    }
}
