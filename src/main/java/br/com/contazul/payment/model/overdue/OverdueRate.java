package br.com.contazul.payment.model.overdue;

import java.math.BigDecimal;

public enum OverdueRate {
    NO_OVERDUE(BigDecimal.ZERO),
    LOWER_OVERDUE_RATE(BigDecimal.valueOf(0.5)),
    HIGHER_OVERDUE_RATE(BigDecimal.valueOf(1.0));

    private BigDecimal rate;

    OverdueRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getRate() {
        return rate;
    }
}
