package br.com.contazul.payment.model.bankslip;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Payments extends JpaRepository<BankSlip, BankSlipId> {

}
