package br.com.contazul.payment.model.bankslip;

import br.com.contazul.payment.model.overdue.Fin;
import br.com.contazul.payment.model.overdue.OverdueRate;
import br.com.contazul.payment.model.overdue.OverdueRates;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

@Entity
public class BankSlip {

    @EmbeddedId
    private BankSlipId id;

    private int totalInCents;

    private LocalDate dueDate;

    private String customer;

    @Column(name = "status_id")
    private Status status;

    @Deprecated
    BankSlip() {
    }

    private BankSlip(BankSlipId id, int totalInCents, LocalDate dueDate, String customer, Status status) {
        this.id = id;
        this.totalInCents = totalInCents;
        this.dueDate = dueDate;
        this.customer = customer;
        this.status = status;
    }

    public BankSlip(int totalInCents, LocalDate dueDate, String customer, Status status) {
        this(BankSlipId.generateRamdomID(), totalInCents, dueDate, customer, status);
    }

    public BankSlipId getId() {
        return this.id;
    }

    public int getTotalInCents() {
        return totalInCents;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public String getCustomer() {
        return customer;
    }

    public Status getStatus() {
        return status;
    }

    public BankSlipDetail createDetails(OverdueRates overdueRates) {
        BigDecimal amount = new BigDecimal(totalInCents)
                .divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN);

        int overdueInDays = getOverdueInDays();
        OverdueRate overdueRate = overdueRates.createFrom(overdueInDays);

        Fin fin = Fin.ZERO;
        if (overdueInDays > 0) {
            BigDecimal interest = amount
                    .multiply(overdueRate.getRate())
                    .multiply(BigDecimal.valueOf(overdueInDays));
            fin = new Fin(interest);
        }

        return new BankSlipDetail(this, fin);
    }

    private int getOverdueInDays() {
        LocalDate today = LocalDate.now();
        return Period.between(dueDate, today).getDays();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankSlip bankSlip = (BankSlip) o;
        return Objects.equals(id, bankSlip.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public BankSlip fromStatus(Status status) {
        return new BankSlip(id, totalInCents, dueDate, customer, status);
    }
}
