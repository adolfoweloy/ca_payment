package br.com.contazul.payment.model.overdue;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Fin {
    public static final Fin ZERO = new Fin(BigDecimal.ZERO);
    private static final int SCALE = 2;

    private final BigDecimal amount;

    public Fin(BigDecimal amount) {
        this.amount = amount;
    }

    public int getAmountInCents() {
        return amount
                .setScale(SCALE, RoundingMode.HALF_EVEN)
                .multiply(BigDecimal.valueOf(100))
                .intValue();
    }

}
