package br.com.contazul.payment.model.bankslip;

import br.com.contazul.payment.validation.InvalidStatusException;

import java.util.Optional;
import java.util.function.Predicate;

import static br.com.contazul.payment.validation.Preconditions.requireNonNull;
import static java.util.Arrays.stream;

public enum Status {

    PENDING(1),
    PAID(2),
    CANCELED(3);

    private Integer id;

    Status(Integer id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Status fromId(Integer statusId) {
        requireNonNull(statusId, () -> new InvalidStatusException("Status ID must not be null."));

        return getStatus(status -> status.id.equals(statusId))
                .orElseThrow(() -> new InvalidStatusException("Status ID " + statusId + " does not exist."));
    }

    public static Status fromName(String name) {
        requireNonNull(name, () -> new InvalidStatusException("Status name must not be null."));

        return getStatus(status -> status.name().equals(name))
                .orElseThrow(() -> new InvalidStatusException("Status name " + name + " does not exist."));
    }

    private static Optional<Status> getStatus(Predicate<? super Status> status) {
        return stream(Status.values())
                .filter(status)
                .findFirst();
    }

}
