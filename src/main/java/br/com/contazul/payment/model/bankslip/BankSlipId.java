package br.com.contazul.payment.model.bankslip;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class BankSlipId implements Serializable {
    private static final long serialVersionUID = 1L;

    private UUID id;

    @Deprecated
    BankSlipId() {}

    private BankSlipId(UUID uuid) {
        this.id = uuid;
    }

    public static BankSlipId fromString(String id) {
        return new BankSlipId(UUID.fromString(id));
    }

    static BankSlipId generateRamdomID() {
        return new BankSlipId(UUID.randomUUID());
    }

    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankSlipId that = (BankSlipId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
