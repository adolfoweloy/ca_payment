package br.com.contazul.payment.model.bankslip;

import br.com.contazul.payment.model.overdue.Fin;

public class BankSlipDetail {
    private final BankSlip bankSlip;
    private final Fin fin;

    BankSlipDetail(BankSlip bankSlip, Fin fin) {
        this.bankSlip = bankSlip;
        this.fin = fin;
    }

    public BankSlip getBankSlip() {
        return bankSlip;
    }

    public Fin getFin() {
        return fin;
    }

}
