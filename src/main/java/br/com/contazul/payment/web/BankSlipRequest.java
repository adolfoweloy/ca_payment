package br.com.contazul.payment.web;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

class BankSlipRequest {
    private String id;

    @NotNull @Min(0)
    private Integer totalInCents;

    @NotNull
    private LocalDate dueDate;

    @NotBlank @NotNull
    private String customer;

    @NotBlank @NotNull
    private String status;

    public Integer getTotalInCents() {
        return totalInCents;
    }

    public void setTotalInCents(Integer totalInCents) {
        this.totalInCents = totalInCents;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
