package br.com.contazul.payment.web;

import br.com.contazul.payment.model.bankslip.Status;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class BankSlipViewDetail {

    @JsonUnwrapped
    private BankSlipView bankSlip;

    private Status status;

    private int fine;

    @Deprecated
    BankSlipViewDetail() { }

    public BankSlipViewDetail(BankSlipView bankSlip, Status status, int fine) {
        this.bankSlip = bankSlip;
        this.status = status;
        this.fine = fine;
    }

    public BankSlipView getBankSlip() {
        return bankSlip;
    }

    public Status getStatus() {
        return status;
    }

    public int getFine() {
        return fine;
    }
}
