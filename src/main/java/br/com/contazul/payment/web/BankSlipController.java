package br.com.contazul.payment.web;

import br.com.contazul.payment.model.bankslip.*;
import br.com.contazul.payment.model.overdue.OverdueRates;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(BankSlipController.ENDPOINT)
class BankSlipController {

    static final String ENDPOINT = "/rest/bankslips";

    private Payments payments;

    BankSlipController(Payments payments) {
        this.payments = payments;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<Void> create(@RequestBody @Valid BankSlipRequest bankSlipRequest) {
        BankSlip newBankSlip = payments.save(createBankSlipFrom(bankSlipRequest));

        URI resourceLocation = UriComponentsBuilder
                .fromPath(ENDPOINT + "/{id}")
                .build(newBankSlip.getId());

        return ResponseEntity.created(resourceLocation).build();
    }

    @GetMapping
    Collection<BankSlipView> list() {
        return payments.findAll().stream()
                .map(this::createBankSlipViewFrom)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<BankSlipViewDetail> details(@PathVariable String id) {
        Optional<BankSlip> optionalBankSlip = payments.findById(BankSlipId.fromString(id));

        return optionalBankSlip
                .map(bankSlip -> {
                    BankSlipDetail detail = bankSlip.createDetails(new OverdueRates());
                    return ResponseEntity.ok(createBankViewDetail(detail));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    ResponseEntity<Void> setStatus(@PathVariable String id,
                                   @RequestBody @Valid StatusChangeRequest statusChangeRequest) {
        Optional<BankSlip> optionalBankSlip = payments.findById(BankSlipId.fromString(id));

        if (optionalBankSlip.isPresent()) {
            BankSlip updatedBankSlip = optionalBankSlip.get()
                    .fromStatus(Status.fromName(statusChangeRequest.getStatus()));
            payments.save(updatedBankSlip);

            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

    private BankSlipViewDetail createBankViewDetail(BankSlipDetail bankSlipDetail) {
        return new BankSlipViewDetail(
                createBankSlipViewFrom(bankSlipDetail.getBankSlip()),
                bankSlipDetail.getBankSlip().getStatus(), bankSlipDetail.getFin().getAmountInCents());
    }

    private BankSlipView createBankSlipViewFrom(BankSlip bankSlip) {
        BankSlipView dto = new BankSlipView();

        dto.setId(bankSlip.getId().toString());
        dto.setCustomer(bankSlip.getCustomer());
        dto.setDueDate(bankSlip.getDueDate());
        dto.setTotalInCents(bankSlip.getTotalInCents());

        return dto;
    }

    private BankSlip createBankSlipFrom(@RequestBody BankSlipRequest bankSlipRequest) {
        return new BankSlip(
                bankSlipRequest.getTotalInCents(),
                bankSlipRequest.getDueDate(),
                bankSlipRequest.getCustomer(),
                Status.fromName(bankSlipRequest.getStatus()));
    }

}
