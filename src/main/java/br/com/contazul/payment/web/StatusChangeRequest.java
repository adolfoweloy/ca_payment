package br.com.contazul.payment.web;

import javax.validation.constraints.NotBlank;

public class StatusChangeRequest {

    @NotBlank
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
