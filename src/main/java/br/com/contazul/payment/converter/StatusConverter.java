package br.com.contazul.payment.converter;

import br.com.contazul.payment.model.bankslip.Status;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Status status) {
        return status.getId();
    }

    @Override
    public Status convertToEntityAttribute(Integer statusId) {
        return Status.fromId(statusId);
    }

}
