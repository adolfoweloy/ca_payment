CREATE TABLE bank_slip (
    id VARCHAR(36) PRIMARY KEY,
    due_date DATE,
    total_in_cents INTEGER,
    customer VARCHAR(200),
    status_id INTEGER
);
