package br.com.contazul.payment.web;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.net.URI;
import java.util.Base64;
import java.util.Collections;

public class OAuth2TokenRequest {

    private TestRestTemplate testRestTemplate;

    public OAuth2TokenRequest(TestRestTemplate testRestTemplate) {
        this.testRestTemplate = testRestTemplate;
    }

    public OAuth2TokenResponse getAccessToken(int port) {
        RequestEntity<MultiValueMap<String, String>> request = new RequestEntity<>(
                getBody(), getHeader(), HttpMethod.POST,
                URI.create("http://localhost:" + port + "/oauth/token"));

        ResponseEntity<OAuth2TokenResponse> oauthResponse = testRestTemplate
                .exchange(request, OAuth2TokenResponse.class);

        return oauthResponse.getBody();
    }

    private HttpHeaders getHeader() {
        String credentials = "contazul:contazul";
        String base64EncodedCredentials = Base64.getEncoder().encodeToString(credentials.getBytes());
        String basicAuthentication = "Basic " + base64EncodedCredentials;

        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", basicAuthentication);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return headers;
    }

    private MultiValueMap<String, String> getBody() {
        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.add("grant_type", "client_credentials");
        form.add("scope", "admin");
        return form;
    }

}
