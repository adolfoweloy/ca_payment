package br.com.contazul.payment.web;

import br.com.contazul.payment.model.bankslip.Status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"development","test_integration"})
@SqlGroup({
    @Sql(scripts = "classpath:sql/create-bank-slips.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
    @Sql(scripts = "classpath:sql/clear-bank-slips.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
})
public class BankSlipControllerTests {

    private static final String BANK_SLIP_ID = "46b113fb-949e-419b-80ed-1c4e1d1cefd8";

    private static final String INVALID_BANK_SLIP_ID = "46b113fb-949e-419b-80ed-1c4e1d1cefdb";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int localServerPort;

    private String accessToken;

    @Before
    public void setup() {
        OAuth2TokenRequest tokenRequest = new OAuth2TokenRequest(restTemplate);
        accessToken = tokenRequest.getAccessToken(localServerPort).getAccessToken();
    }

    @Test
    public void shouldSaveABankSlip() {
        ResponseEntity<Void> response = restTemplate.postForEntity(
                BankSlipController.ENDPOINT, createHttpRequestWithBody(createBankSlipRequest()), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getHeaders().containsKey(HttpHeaders.LOCATION)).isTrue();
    }

    @Test
    public void shouldReturnStatus400WhenBankSlipIsNotProvided() {
        ResponseEntity<Void> response = restTemplate.exchange(
                BankSlipController.ENDPOINT,
                HttpMethod.POST, createHeaderEntityWithEmptyBody(), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturnStatus422WhenCustomerIsNotProvided() {
        BankSlipRequest bankSlipRequest = createBankSlipRequest();
        bankSlipRequest.setCustomer(null);

        ResponseEntity<Void> response = restTemplate.exchange(
                BankSlipController.ENDPOINT,
                HttpMethod.POST, createHttpRequestWithBody(bankSlipRequest), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void shouldReturnStatus422WhenDueDateIsNotProvided() {
        BankSlipRequest bankSlipRequest = createBankSlipRequest();
        bankSlipRequest.setDueDate(null);

        ResponseEntity<Void> response = restTemplate.exchange(
                BankSlipController.ENDPOINT,
                HttpMethod.POST, createHttpRequestWithBody(bankSlipRequest), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void shouldReturnStatus422WhenTotalAmountInCentsIsNotProvided() {
        BankSlipRequest bankSlipRequest = createBankSlipRequest();
        bankSlipRequest.setTotalInCents(null);

        ResponseEntity<Void> response = restTemplate.exchange(
                BankSlipController.ENDPOINT,
                HttpMethod.POST, createHttpRequestWithBody(bankSlipRequest), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void shouldReturnAllBankSlips() {
        ParameterizedTypeReference<List<BankSlipView>> responseType = new ParameterizedTypeReference<List<BankSlipView>>() {};
        ResponseEntity<List<BankSlipView>> response = restTemplate.exchange(
                URI.create(BankSlipController.ENDPOINT),
                HttpMethod.GET, createHeaderEntityWithEmptyBody(), responseType);

        List<BankSlipView> bankSlips = response.getBody();
        assertThat(bankSlips.size()).isEqualTo(3);

        BankSlipView firstBankSlipRequest = bankSlips.get(0);
        assertThat(firstBankSlipRequest.getId()).isNotNull();
        assertThat(firstBankSlipRequest.getCustomer()).isNotNull();
        assertThat(firstBankSlipRequest.getDueDate()).isNotNull();
        assertThat(firstBankSlipRequest.getTotalInCents()).isNotNull();
    }

    @Test
    public void shouldRetrieveBankSlipDetail() {
        ResponseEntity<BankSlipViewDetail> response = restTemplate.exchange(
                URI.create(BankSlipController.ENDPOINT + "/" + BANK_SLIP_ID),
                HttpMethod.GET, createHeaderEntityWithEmptyBody(), BankSlipViewDetail.class);

        assertThat(response.getStatusCode()).isSameAs(HttpStatus.OK);

        BankSlipViewDetail detail = response.getBody();
        assertThat(detail).isNotNull();
    }

    @Test
    public void shouldReturnNotFoundResponseWhenBankSlipDoesntExist() {
        ResponseEntity<BankSlipViewDetail> response = restTemplate.exchange(
                URI.create(BankSlipController.ENDPOINT + "/" + INVALID_BANK_SLIP_ID),
                HttpMethod.GET, createHeaderEntityWithEmptyBody(), BankSlipViewDetail.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldReturnBadRequestWhenSendingAnInvalidUUID() {
        ResponseEntity<Void> response = restTemplate.exchange(
                URI.create(BankSlipController.ENDPOINT + "/tesla"),
                HttpMethod.GET, createHeaderEntityWithEmptyBody(), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldChangeBankSlipStatusToPaid() {
        ResponseEntity<Void> response = restTemplate.exchange(
                URI.create(BankSlipController.ENDPOINT + "/" + BANK_SLIP_ID),
                HttpMethod.PUT, createHttpRequestForPaidStatus(), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldChangeBankSlipStatusToCanceled() {
        ResponseEntity<Void> response = restTemplate.exchange(
                URI.create(BankSlipController.ENDPOINT + "/" + BANK_SLIP_ID),
                HttpMethod.PUT, createHttpRequestForCanceledStatus(), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private BankSlipRequest createBankSlipRequest() {
        BankSlipRequest request = new BankSlipRequest();
        request.setTotalInCents(1000);
        request.setDueDate(LocalDate.now());
        request.setCustomer("John Doe");
        request.setStatus(Status.PENDING.name());
        return request;
    }

    private HttpEntity<StatusChangeRequest> createHttpRequestForPaidStatus() {
        StatusChangeRequest request = new StatusChangeRequest();
        request.setStatus(Status.PAID.name());
        return new HttpEntity<>(request, createHttpHeaders());
    }

    private HttpEntity<StatusChangeRequest>  createHttpRequestForCanceledStatus() {
        StatusChangeRequest request = new StatusChangeRequest();
        request.setStatus(Status.CANCELED.name());
        return new HttpEntity<>(request, createHttpHeaders());
    }

    private <T> HttpEntity<T> createHttpRequestWithBody(T body) {
        return new HttpEntity<>(body, createHttpHeaders());
    }

    private HttpEntity<Void> createHeaderEntityWithEmptyBody() {
        return new HttpEntity<>(createHttpHeaders());
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String bearerToken = "Bearer " + accessToken;
        headers.add(HttpHeaders.AUTHORIZATION, bearerToken);

        return headers;
    }
}