## Running the application

Just type the following command within the project's directory

```bash
SPRING_PROFILES_ACTIVE=development ./gradlew bootRun
```

## Design decisions

- I have decided to not create something like a DTO to decouple the REST API interface from domain model
because it would just duplicate data structures at the first moment. I prefer to wait for some change on our domain 
to start decoupling. That's not a decision from today's Adolfo (I defer it to Adolfo from future).

## Technical decisions

### Spring Boot

- Spring Boot and related frameworks were chosen given my experience creating backend applications with Java.

### Gradle

- All generated gradle wrapper files were committed to avoid developers to worry about
which [Gradle version to use](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.wrapper.Wrapper.html), 
where to download it from, to _minimize time and cost for 
new developers joining the project_ as described by [Twelve Factor app](https://12factor.net/) methodology.

## Running examples

Before running the examples, you need to create an access token so you can interact with bankslips API. That's because
this API is OAuth 2.0 protected using Client Credentials grant type.

So to request for an access token just run the following curl command:

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" \
-u contazul:contazul -d "grant_type=client_credentials" \
"http://localhost:8080/oauth/token"
```

Then you should receive something like follows (with a different access token):

```json
{
	"access_token": "3453194c-0ce1-4784-acfa-78ac7d52a2c7",
	"token_type": "bearer",
	"expires_in": 43199,
	"scope": "admin"
}
```

> To start using the API, just copy the issued access token and use it as a Bearer token within the Authorization HTTP Header.
> All the following commands are sending an access token (you must use your own access token when running the tests).

- To create a bank slip
```bash
curl -v -X POST "http://localhost:8080/rest/bankslips" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer 3453194c-0ce1-4784-acfa-78ac7d52a2c7" \
-d "{\"total_in_cents\": 1000, \"due_date\": \"2018-10-10\", \"customer\": \"john doe\", \"status\":\"PENDING\"}"
```

- To list all bank slips
```bash
curl -v -H "Authorization: Bearer 3453194c-0ce1-4784-acfa-78ac7d52a2c7" \
"http://localhost:8080/rest/bankslips"
```

- To list details given a bank slip
```bash
curl -v -H "Authorization: Bearer 3453194c-0ce1-4784-acfa-78ac7d52a2c7" \
"http://localhost:8080/rest/bankslips/53294c39-97c9-4015-b1e9-e6274e045103"
```

- To pay a bank slip
```bash
curl -v -X PUT -H "Content-Type: application/json" \
-H "Authorization: Bearer 3453194c-0ce1-4784-acfa-78ac7d52a2c7" \
-d "{\"status\": \"PAID\"}" "http://localhost:8080/rest/bankslips/53294c39-97c9-4015-b1e9-e6274e045103"
```

- To cancel a bank slip
```bash
curl -v -X PUT -H "Content-Type: application/json" \
-H "Authorization: Bearer 3453194c-0ce1-4784-acfa-78ac7d52a2c7" \
-d "{\"status\": \"CANCELED\"}" "http://localhost:8080/rest/bankslips/53294c39-97c9-4015-b1e9-e6274e045103"
```

Started working with git worktree! nice!